/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/index.js":
/*!**********************!*\
  !*** ./app/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modules_display_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/display.js */ \"./app/modules/display.js\");\n/* harmony import */ var _modules_workItems_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/workItems.js */ \"./app/modules/workItems.js\");\n\r\n\r\n\r\n\r\n\r\nlet display = new _modules_display_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\r\nnew _modules_workItems_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"](display);\n\n//# sourceURL=webpack:///./app/index.js?");

/***/ }),

/***/ "./app/modules/display.js":
/*!********************************!*\
  !*** ./app/modules/display.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass Display {\r\n\tconstructor() {\r\n\t\tthis.form = document.forms.displayForm;\r\n\t\tthis.id = 1;\r\n\t\tthis.initSubmitEvent();\r\n\t}\r\n\r\n\tinitSubmitEvent() {\r\n\t\tthis.form.addEventListener('submit', event => {\r\n\t\t\tevent.preventDefault();\r\n\t\t\tthis.openModalWin();\r\n\t\t\tlet infoTarget = document.getElementById('modalContent');\r\n\t\t\t\tinfoTarget.innerHTML = null;\r\n\t\t\tlet existedElements = this.getElements();\r\n\t\t\t\texistedElements.forEach(el=>{\r\n\r\n\t\t\t\t\tlet item = el.children[1];\r\n\t\t\t\t\t\r\n\t\t\t\t\tlet iValue;\r\n\t\t\t\t\tif (item.type == 'checkbox' || item.type == 'radio') {\r\n\t\t\t\t\t\tiValue = item.checked;\r\n\t\t\t\t\t} else {\r\n\t\t\t\t\t\tiValue = item.value;\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\tlet iName = item.name;\r\n\t\t\t\t\tlet iType = el.dataset.type;\r\n\r\n\r\n\t\t\t\t\tlet infoBlock = document.createElement('li');\r\n\t\t\t\t\t\tinfoBlock.className = 'modalItem';\r\n\t\t\t\t\tlet pType = document.createElement('p');\r\n\t\t\t\t\t\tpType.innerHTML = `type of element: <span class='modText-1'> ${iType}</span>`;\r\n\t\t\t\t\tlet pName = document.createElement('p');\r\n\t\t\t\t\t\tpName.innerHTML = `name of element: <span class='modText-1'> ${iName}</span>`;\r\n\t\t\t\t\tlet pValue = document.createElement('p');\r\n\t\t\t\t\t\tpValue.innerHTML = `value: <span class='modText-2'>${iValue}</span`;\r\n\r\n\t\t\t\t\tinfoBlock.appendChild(pType);\r\n\t\t\t\t\tinfoBlock.appendChild(pName);\r\n\t\t\t\t\tinfoBlock.appendChild(pValue);\r\n\r\n\t\t\t\t\tinfoTarget.appendChild(infoBlock);\r\n\t\t\t\t})\r\n\t\t})\r\n\t}\r\n\r\n\topenModalWin() {\r\n\t\tlet wrap = document.getElementById('modal_wrap');\r\n\t\t\twrap.classList.add('modal_show');\r\n\t\t\twrap.style.display = 'block';\r\n\r\n\t\tlet closeBtn = wrap.querySelector('.close');\r\n\r\n\t\tcloseBtn.addEventListener('click', e=>{\r\n\t\t\twrap.style.display = 'none';\r\n\t\t});\r\n\t}\r\n\r\n\tcreateElem( name, type ) {\r\n\t\tlet thisname = (name === 'default') ? (name = name + this.id) : name;\r\n\r\n\t\tlet label = document.createElement('label');\r\n\t\t\tlabel.className = 'displayLabel';\r\n\t\t\tlabel.id = this.id;\r\n\t\t\tlabel.setAttribute('data-type', type);\r\n\t\t\tlabel.setAttribute('data-name', name);\r\n\t\t\tlabel.innerHTML = `<span style=\"color: #2A6E2A\"> element \"${type}\"; name: ${name}</span>`;\r\n\t\tlet elem;\r\n\r\n\t\tif (type === 'textarea') {\r\n\t\t\telem = document.createElement('textarea');\r\n\t\t\telem.setAttribute('name', thisname);\r\n\t\t\telem.className = 'forDisplay';\r\n\t\t} else {\r\n\t\t\telem = document.createElement('input');\r\n\t\t\telem.setAttribute('name', thisname);\r\n\t\t\telem.setAttribute('type', type);\r\n\t\t\telem.className = 'forDisplay';\r\n\t\t}\r\n\t\t\r\n\t\tlabel.appendChild(elem);\r\n\t\tthis.form.appendChild( label );\r\n\t\tthis.id++;\r\n\t}\r\n\r\n\tgetElements() {\r\n\t\treturn Array.from(this.form.children);\r\n\t}\r\n\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Display);\n\n//# sourceURL=webpack:///./app/modules/display.js?");

/***/ }),

/***/ "./app/modules/workItems.js":
/*!**********************************!*\
  !*** ./app/modules/workItems.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction hideAll(arr, itemIdent, removeClss) {\r\n\tarr.forEach(el=>{\r\n\t\tlet item = el.querySelector(itemIdent);\r\n\t\titem.classList.remove(removeClss);\r\n\t})\r\n}\r\n\r\nclass workItems {\r\n\tconstructor(display){\r\n\t\tthis.display = display;\r\n\t\tthis.items = Array.from(document.querySelector('.sideBar_list').children);\r\n\t\tthis.items.forEach(el=>{\r\n\t\t\tthis.initHandler(el);\r\n\t\t})\r\n\t}\r\n\r\n\r\n\tinitHandler(listItem) {\r\n\t\tlet listTitle = listItem.querySelector('.listTitle');\r\n\t\tlet workStuff = listItem.querySelector('.workStuff');\r\n\t\t\r\n\t\tlistTitle.addEventListener('click', event=>{\r\n\t\t\tif (listTitle.classList.contains('light')) {\r\n\t\t\t\tthis.createExistedList();\r\n\t\t\t}\r\n\r\n\r\n\t\t\tif (workStuff.classList.contains('show')) {\r\n\t\t\t\tworkStuff.classList.remove('show');\r\n\t\t\t} else {\r\n\t\t\t\thideAll(this.items, '.workStuff', 'show');\r\n\t\t\t\tworkStuff.classList.add('show');\r\n\t\t\t\tlistItem.classList.add('bordered');\r\n\t\t\t}\r\n\t\t});\r\n\r\n\t\tlet itemForm = listItem.querySelector('form');\r\n\t\t\titemForm.addEventListener('submit', event=>{\r\n\t\t\t\tevent.preventDefault();\r\n\r\n\t\t\t\tlet newItem_name = itemForm.nameSetter.value || 'no-name';\r\n\t\t\t\tlet itemType = listItem.dataset.type;\r\n\r\n\t\t\t\tthis.display.createElem(newItem_name, itemType);\r\n\r\n\t\t\t\titemForm.nameSetter.value = null;\r\n\t\t\t})\r\n\t\t\r\n\t}\r\n\r\n\tcreateExistedList() {\r\n\t\tlet listTarget = document.getElementById('listTarget');\r\n\t\t\tlistTarget.innerText = null;\r\n\t\tlet displayElements = this.display.getElements();\r\n\r\n\t\tlet delBtn = document.getElementById('delExistBtn');\r\n\t\tif (displayElements.length < 1) {\r\n\t\t\tlistTarget.innerText = 'no elements';\r\n\t\t\tdelBtn.style.display = 'none';\r\n\t\t} else {\r\n\t\t\tdelBtn.style.display = 'block';\r\n\t\t}\r\n\r\n\t\tlet elemsForDelete = [];\r\n\t\t\t\r\n\t\t\tdelBtn.addEventListener('click', event=>{\r\n\t\t\t\telemsForDelete.forEach(el=>{\r\n\t\t\t\t\tel.remove();\r\n\t\t\t\t\tthis.createExistedList();\r\n\t\t\t\t})\r\n\t\t\t})\r\n\r\n\r\n\t\tdisplayElements.forEach(el=>{\r\n\t\t\tlet elType = el.dataset.type;\r\n\t\t\tlet elName = el.dataset.name;\r\n\r\n\t\t\tlet newExItemLabel = document.createElement('label');\r\n\t\t\t\tnewExItemLabel.classList = 'ex_list';\r\n\t\t\tlet exItemText = document.createElement('span');\r\n\t\t\t\texItemText.innerText = `type: ${elType}; name: ${elName}`;\r\n\t\t\tlet exCheckbox = document.createElement('input');\r\n\t\t\t\texCheckbox.type = 'checkbox';\r\n\t\t\t\texCheckbox.setAttribute('name', 'select');\r\n\t\t\t\texCheckbox.addEventListener('change', event=>{\r\n\t\t\t\t\tif (exCheckbox.checked) {\r\n\t\t\t\t\t\tel.style.background = '#808080';\r\n\t\t\t\t\t\telemsForDelete.push(el);\r\n\t\t\t\t\t} else {\r\n\t\t\t\t\t\tel.style.background = '';\r\n\t\t\t\t\t\telemsForDelete.forEach(arrItem=>{\r\n\t\t\t\t\t\t\tif (arrItem.id == el.id) {\r\n\t\t\t\t\t\t\t\tlet ind = elemsForDelete.indexOf(arrItem);\r\n\t\t\t\t\t\t\t\telemsForDelete.splice(ind, 1);\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t})\r\n\t\t\t\t\t}\r\n\t\t\t\t})\r\n\r\n\t\t\t\tnewExItemLabel.appendChild(exItemText);\r\n\t\t\t\tnewExItemLabel.appendChild(exCheckbox);\r\n\r\n\t\t\t\tlistTarget.appendChild(newExItemLabel);\r\n\t\t})\r\n\t}\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (workItems);\n\n//# sourceURL=webpack:///./app/modules/workItems.js?");

/***/ })

/******/ });