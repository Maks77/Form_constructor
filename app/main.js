function hideAll(arr, itemIdent, removeClss) {
	arr.forEach(el=>{
		let item = el.querySelector(itemIdent);
		item.classList.remove(removeClss);
	})
}

class Display {
	constructor() {
		this.form = document.forms.displayForm;
		this.id = 1;
		this.initSubmitEvent();
	}

	initSubmitEvent() {
		this.form.addEventListener('submit', event => {
			event.preventDefault();
			this.openModalWin();
			let infoTarget = document.getElementById('modalContent');
				infoTarget.innerHTML = null;
			let existedElements = this.getElements();
				existedElements.forEach(el=>{
					let item = el.children[1];
					let iValue;
					if (item.type == 'checkbox' || item.type == 'radio') {
						iValue = item.checked;
					} else {
						iValue = item.value;
					}

					let iName = item.name;
					let iType = el.dataset.type;


					let infoBlock = document.createElement('li');
						infoBlock.className = 'modalItem';
					let pType = document.createElement('p');
						pType.innerHTML = `type of element: <span class='modText-1'> ${iType}</span>`;
					let pName = document.createElement('p');
						pName.innerHTML = `name of element: <span class='modText-1'> ${iName}</span>`;
					let pValue = document.createElement('p');
						pValue.innerHTML = `value: <span class='modText-2'>${iValue}</span`;

					infoBlock.appendChild(pType);
					infoBlock.appendChild(pName);
					infoBlock.appendChild(pValue);

					infoTarget.appendChild(infoBlock);
				})
		})
	}

	openModalWin() {
		let wrap = document.getElementById('modal_wrap');
			wrap.classList.add('modal_show');
			wrap.style.display = 'block';

		let closeBtn = wrap.querySelector('.close');

		closeBtn.addEventListener('click', e=>{
			wrap.style.display = 'none';
		});
	}

	createElem( name, type ) {
		let thisname = (name === 'default') ? (name = name + this.id) : name;

		let label = document.createElement('label');
			label.className = 'displayLabel';
			label.id = this.id;
			label.setAttribute('data-type', type);
			label.setAttribute('data-name', name);
			label.innerHTML = `<span style="color: #2A6E2A"> element "${type}"; name: ${name}</span>`;
		let elem;

		if (type === 'textarea') {
			elem = document.createElement('textarea');
			elem.setAttribute('name', thisname);
			elem.className = 'forDisplay';
		} else {
			elem = document.createElement('input');
			elem.setAttribute('name', thisname);
			elem.setAttribute('type', type);
			elem.className = 'forDisplay';
		}
		
		label.appendChild(elem);
		this.form.appendChild( label );
		this.id++;
	}

	getElements() {
		return Array.from(this.form.children);
	}

}


class workItems {
	constructor(){
		this.display = new Display();
		this.items = Array.from(document.querySelector('.sideBar_list').children);
		this.items.forEach(el=>{
			this.initHandler(el);
		})
	}


	initHandler(listItem) {
		let listTitle = listItem.querySelector('.listTitle');
		let workStuff = listItem.querySelector('.workStuff');
		
		listTitle.addEventListener('click', event=>{
			if (listTitle.classList.contains('light')) {
				this.createExistedList();
			}


			if (workStuff.classList.contains('show')) {
				workStuff.classList.remove('show');
			} else {
				hideAll(this.items, '.workStuff', 'show');
				workStuff.classList.add('show');
				listItem.classList.add('bordered');
			}
		});

		let itemForm = listItem.querySelector('form');
			itemForm.addEventListener('submit', event=>{
				event.preventDefault();

				let newItem_name = itemForm.nameSetter.value || 'no-name';
				let itemType = listItem.dataset.type;

				this.display.createElem(newItem_name, itemType);

				itemForm.nameSetter.value = null;
			})
		
	}

	createExistedList() {
		let listTarget = document.getElementById('listTarget');
			listTarget.innerText = null;
		let displayElements = this.display.getElements();

		let delBtn = document.getElementById('delExistBtn');
		if (displayElements.length < 1) {
			listTarget.innerText = 'no elements';
			delBtn.style.display = 'none';
		} else {
			delBtn.style.display = 'block';
		}

		let elemsForDelete = [];
			
			delBtn.addEventListener('click', event=>{
				elemsForDelete.forEach(el=>{
					el.remove();
					this.createExistedList();
				})
			})


		displayElements.forEach(el=>{
			let elType = el.dataset.type;
			let elName = el.dataset.name;

			let newExItemLabel = document.createElement('label');
				newExItemLabel.classList = 'ex_list';
			let exItemText = document.createElement('span');
				exItemText.innerText = `type: ${elType}; name: ${elName}`;
			let exCheckbox = document.createElement('input');
				exCheckbox.type = 'checkbox';
				exCheckbox.setAttribute('name', 'select');
				exCheckbox.addEventListener('change', event=>{
					if (exCheckbox.checked) {
						el.style.background = '#808080';
						elemsForDelete.push(el);
					} else {
						el.style.background = '';
						elemsForDelete.forEach(arrItem=>{
							if (arrItem.id == el.id) {
								let ind = elemsForDelete.indexOf(arrItem);
								elemsForDelete.splice(ind, 1);
							}
						})
					}
				})

				newExItemLabel.appendChild(exItemText);
				newExItemLabel.appendChild(exCheckbox);

				listTarget.appendChild(newExItemLabel);
		})
	}
}

new workItems();
