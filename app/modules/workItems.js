function hideAll(arr, itemIdent, removeClss) {
	arr.forEach(el=>{
		let item = el.querySelector(itemIdent);
		item.classList.remove(removeClss);
	})
}

class workItems {
	constructor(display){
		this.display = display;
		this.items = Array.from(document.querySelector('.sideBar_list').children);
		this.items.forEach(el=>{
			this.initHandler(el);
		})
	}


	initHandler(listItem) {
		let listTitle = listItem.querySelector('.listTitle');
		let workStuff = listItem.querySelector('.workStuff');
		
		listTitle.addEventListener('click', event=>{
			if (listTitle.classList.contains('light')) {
				this.createExistedList();
			}


			if (workStuff.classList.contains('show')) {
				workStuff.classList.remove('show');
			} else {
				hideAll(this.items, '.workStuff', 'show');
				workStuff.classList.add('show');
				listItem.classList.add('bordered');
			}
		});

		let itemForm = listItem.querySelector('form');
			itemForm.addEventListener('submit', event=>{
				event.preventDefault();

				let newItem_name = itemForm.nameSetter.value || 'no-name';
				let itemType = listItem.dataset.type;

				this.display.createElem(newItem_name, itemType);

				itemForm.nameSetter.value = null;
			})
		
	}

	createExistedList() {
		let listTarget = document.getElementById('listTarget');
			listTarget.innerText = null;
		let displayElements = this.display.getElements();

		let delBtn = document.getElementById('delExistBtn');
		if (displayElements.length < 1) {
			listTarget.innerText = 'no elements';
			delBtn.style.display = 'none';
		} else {
			delBtn.style.display = 'block';
		}

		let elemsForDelete = [];
			
			delBtn.addEventListener('click', event=>{
				elemsForDelete.forEach(el=>{
					el.remove();
					this.createExistedList();
				})
			})


		displayElements.forEach(el=>{
			let elType = el.dataset.type;
			let elName = el.dataset.name;

			let newExItemLabel = document.createElement('label');
				newExItemLabel.classList = 'ex_list';
			let exItemText = document.createElement('span');
				exItemText.innerText = `type: ${elType}; name: ${elName}`;
			let exCheckbox = document.createElement('input');
				exCheckbox.type = 'checkbox';
				exCheckbox.setAttribute('name', 'select');
				exCheckbox.addEventListener('change', event=>{
					if (exCheckbox.checked) {
						el.style.background = '#808080';
						elemsForDelete.push(el);
					} else {
						el.style.background = '';
						elemsForDelete.forEach(arrItem=>{
							if (arrItem.id == el.id) {
								let ind = elemsForDelete.indexOf(arrItem);
								elemsForDelete.splice(ind, 1);
							}
						})
					}
				})

				newExItemLabel.appendChild(exItemText);
				newExItemLabel.appendChild(exCheckbox);

				listTarget.appendChild(newExItemLabel);
		})
	}
}

export default workItems;